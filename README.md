# Project 1-Survey_Phase 1

Create a website the contains the following:

- Landing page:
A place for users to sign up for user accounts (username, password, email address)
A place to login next to, under, etc, on the same page as the sign-up.

- Main page (after login):
User's Surveys
Every Survey title has: title, description, date, created-by

- Form validation for user sign-up:
No field can be blank
E-mail field must be a valid email
Password fields must match and display to the user while typing whether or not they currently match given their current contents.

- Logging in:
An admin can view surveys as well as create them.
Your site must be written entirely in HTML5 using semantic elements.

Use CSS to make it look good.
Use good coding practices.
