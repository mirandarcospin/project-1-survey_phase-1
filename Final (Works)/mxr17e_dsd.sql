-- FOR ME ONLY: \i c:/Users/miran/OneDrive/Desktop/ACU/7-SEMESTER/IT325/mxr17e_dsd.sql
--
-- mxr17e_dsd.sql
-- IT 325
--
-- Database Schema and Data # 2
--
-- Miranda Ramirez Cospin
--

\c postgres
drop database if exists mxr17e_dsd;
CREATE DATABASE mxr17e_dsd encoding 'UTF-8';
\c mxr17e_dsd;

-- QUESTIONS 60 --
drop table if exists question;
CREATE TABLE question (
  id int primary key,
  quest text,
  question_type char(2),
  question_responses int,
  survey_id int
);
-- Multiple choice = MC. Short answer = SA. True/False = TF. Checkbox = CB. Dropdown = DD.
insert into question (id, quest, question_type, question_responses, survey_id) values
  (1, 'Are you a student at ACU?', 'TF', 1, 1),
  (2, 'What is you major?', 'SA', 12, 1),
  (3, 'What is your classification?', 'MC', 3, 1),
  (4, 'Choose pizza toppings:', 'CB', 6, 1),
  (5, 'Have you been out of the United States?', 'TF', 1, 2),
  (6, 'Where is the location you most often visit?', 'SA', 12, 2),
  (7, 'How many countries have you been to?', 'MC', 4, 2),
  (8, 'Which of these places have you been to?', 'CB', 7, 2),
  (9, 'Are you a student at ACU?', 'TF', 1, 3),
  (10, 'Tell us how you feel about your experience here?', 'SA', 12, 3),
  (11, 'What is your classification?', 'MC', 5, 3),
  (12, 'Which of these do you think could be improved on?', 'CB', 8, 3),
  (13, 'Simple, are you happy?', 'TF', 1, 4),
  (14, 'Tell me what makes you happy?', 'SA', 12, 4),
  (15, 'What best describes your source of happiness?', 'MC', 5, 4),
  (16, 'What contributes to your happiness?', 'CB', 8, 4),
  (17, 'Do you attend chapel?', 'TF', 1, 5),
  (18, 'What do you like about chapel?', 'SA', 12, 5),
  (19, 'How many days a week do you attend chapel?', 'MC', 4, 5),
  (20, 'Which chapels have you attended?', 'CB', 7, 5),
  (21, 'Do you prefer to cook or buy food?', 'MC', 3, 6),
  (22, 'Write any food allergies:', 'SA', 12, 6),
  (23, 'Your favorite food can be found in Abilene.', 'TF', 2, 6),
  (24, 'Choose your favorite restaurants:', 'CB', 7, 6),
  (25, 'Do you spend a lot of time on hobbies?', 'MC', 5, 7),
  (26, 'What got you interested in your favorite hobby?', 'SA', 12, 7),
  (27, 'Are you satisfied with your current hobbies?', 'TF', 1, 7),
  (28, 'What most describes your current hobbies?', 'CB', 6, 7),
  (29, 'Which state is the largest', 'MC', 4, 8),
  (30, 'Where does your knowledge of geography come from?', 'SA', 12, 8),
  (31, 'Is Texas bigger than California?', 'TF', 1, 8),
  (32, 'Which of these states are on the west coast?', 'CB', 7, 8),
  (33, 'What personality description best describes you?', 'MC', 3, 9),
  (34, 'What is your favorite thing about your personality?', 'SA', 12, 9),
  (35, 'Are you an outgoing person?', 'TF', 2, 9),
  (36, 'Which of these traits apply to you?', 'CB', 8, 9),
  (37, 'Which part of America are you from?', 'MC', 5, 10),
  (38, 'What is unique about your culture?', 'SA', 12, 10),
  (39, 'Do you think your culture is much different from others?', 'TF', 1, 10),
  (40, 'What aspects make your culture unique?', 'CB', 7, 10),
  (41, 'Do you have any coupons for this store?', 'MC', 3, 11),
  (42, 'What are you planning on buying?', 'SA', 12, 11),
  (43, 'This is the best city to go shopping!', 'TF', 1, 11),
  (44, 'What stores in this city do you know:', 'CB', 6, 11),
  (45, 'What is your favorite genre?', 'MC', 4, 12),
  (46, 'List what makes a show good for you.', 'SA', 12, 12),
  (47, 'Have you watched The Office?', 'TF', 2, 12),
  (48, 'Which of these shows have you watched?', 'CB', 8, 12),
  (49, 'Which hours are you most productive?', 'MC', 4, 13),
  (50, 'Why are you more active during these hours?', 'SA', 12, 13),
  (51, 'Are you a morning person?', 'TF', 1, 13),
  (52, 'Pick the times of the day you sleep.', 'CB', 8, 13),
  (53, 'Which of these methods best describes you?', 'MC', 5, 14),
  (54, 'What inspires you to be productive?', 'SA', 12, 14),
  (55, 'Are you a hard worker.', 'TF', 2, 14),
  (56, 'What is challenging for you?', 'CB', 7, 14),
  (57, 'What is ethically right?', 'MC', 4, 15),
  (58, 'What makes someone an ethical person?', 'SA', 12, 15),
  (59, 'Should someone steal for their family?', 'TF', 1, 15),
  (60, 'What makes it okay to be unethical?', 'CB', 8, 15);
-- TABLE: Show questions
select * from question order by id;

-- SURVEY 15 --
drop table if exists survey;
CREATE TABLE survey (
  id serial primary key,
  title text,
  description text,
  author_id int,
  active char
);
insert into survey (id, title, description, author_id, active) values
  (1, 'S1 - Pizza Party ITC', 'description for survey 1', 1, 'Y'),
  (2, 'S2 - Pizza Party CS', 'description for survey 2', 1, 'Y'),
  (3, 'S3 - Pizza Party DET', 'description for survey 3', 1, 'N'),
  (4, 'S4 - Pizza Party IS', 'description for survey 4', 2, 'N'),
  (5, 'S5 - Pizza Party MKTG', 'description for survey 5', 2, 'N'),
  (6, 'S6 - Favorite Food Monday Class', 'description for survey 6', 3, 'Y'),
  (7, 'S7 - Favorite Food Tuesday Class', 'description for survey 7', 3, 'N'),
  (8, 'S8 - Favorite Food Wednesday Class', 'description for survey 8', 2, 'Y'),
  (9, 'S9 - Favorite Food Thursday Class', 'description for survey 9', 2, 'N'),
  (10, 'S10 - Favorite Food Friday Class', 'description for survey 10', 4, 'Y'),
  (11, 'S11 - Shopping Stores New York', 'description for survey 11', 4, 'Y'),
  (12, 'S12 - Shopping Stores Malibu', 'description for survey 12', 3, 'Y'),
  (13, 'S13 - Shopping Stores Las Vegas', 'description for survey 13', 4, 'Y'),
  (14, 'S14 - Shopping Stores Milan', 'description for survey 14', 3, 'Y'),
  (15, 'S15 - Shopping Stores Paris', 'description for survey 15', 4, 'Y');
-- TABLE: Show surveys "created"
select * from survey order by id;
-- TABLE: Show active surveys
select * from survey where active='Y'
order by id;

-- SURVEY_AND_QUESTION --
drop table if exists survey_question;
create table survey_question (
  s_id int,
  q_id int
);
insert into survey_question (s_id, q_id) values
(1, 1),(1, 2),(1, 3),(1, 4),
(2, 5),(2, 6),(2, 7),(2, 8),
(3, 9),(3, 10),(3, 11),(3, 12),
(4, 13),(4, 14),(4, 15),(4, 16),
(5, 17),(5, 18),(5, 19),(5, 20),
(6, 21),(6, 22),(6, 23),(6, 24),
(7, 25),(7, 26),(7, 27),(7, 28),
(8, 29),(8, 30),(8, 31),(8, 32),
(9, 33),(9, 34),(9, 35),(9, 36),
(10, 37),(10, 38),(10, 39),(10, 40),
(11, 41),(11, 42),(11, 43),(11, 44),
(12, 45),(12, 46),(12, 47),(12, 48),
(13, 49),(13, 50),(13, 51),(13, 52),
(14, 53),(14, 54),(14, 55),(14, 56),
(15, 57),(15, 58),(15, 59),(15, 50);

-- RESPONSES --
drop table if exists question_type;
CREATE TABLE question_type (
  qcode int,
  qtype char(2),
  description text,
  primary key (qtype, qcode)
);
insert into question_type (qcode, qtype, description) values
  (1, 'TF', 'True'),
  (2, 'TF', 'False'),
  (3, 'MC', 'MC-Option 1'),
  (4, 'MC', 'MC-Option 2'),
  (5, 'MC', 'MC-Option 3'),
  (6, 'CB', 'CB-Option 1'),
  (7, 'CB', 'CB-Option 2'),
  (8, 'CB', 'CB-Option 3'),
  (9, 'DD', 'DD-Option 1'),
  (10, 'DD', 'DD-Option 2'),
  (11, 'DD', 'DD-Option 3'),
  (12, 'SA', 'Short answer from user');
--TABLE: Show all types of responses for the questions
select * from question_type order by qcode;

-- TABLE: Questions and responses
select q.id, q.quest, q.question_type, r.description, r.qcode
from question q inner join question_type r on (q.question_responses = r.qcode)
order by q.id;

-- authorS 4 --
drop table if exists author;
CREATE TABLE author(
  id int primary key,
  author_name text,
  is_author char,
  email text,
  password text
);
insert into author(id, author_name, is_author, email, password) values
  (1, 'Test', 'Y', 'abc@acu.edu','boss'),
  (2, 'Miranda', 'Y', 'miranda@acu.edu','miranda1'),
  (3, 'Fisher', 'Y', 'fisher@acu.edu','fisher1'),
  (4, 'Brent', 'Y', 'brent@acu.edu','blee');

--TABLE: Show all authors
select * from author order by id;
--TABLE: Show who (which author) made the different surveys
select s.id, s.title, s.description, a.id, a.author_name, a.is_author
from survey s inner join author a on (s.author_id = a.id)
order by s.id;

-- USERS 40 --
drop table if exists users;
CREATE TABLE users (
  user_id int primary key,
  user_name text,
  email text,
  is_author char
);
insert into users (user_id, user_name, email, is_author) values
  (1, 'user1', 'user1@acu.edu', 'N'),
  (2, 'user2', 'user2@acu.edu', 'N'),
  (3, 'user3', 'user3@acu.edu', 'N'),
  (4, 'user4', 'user4@acu.edu', 'N'),
  (5, 'user5', 'user5@acu.edu', 'N'),
  (6, 'user6', 'user6@acu.edu', 'N'),
  (7, 'user7', 'user7@acu.edu', 'N'),
  (8, 'user8', 'user8@acu.edu', 'N'),
  (9, 'user9', 'user9@acu.edu', 'N'),
  (10, 'user10', 'user10@acu.edu', 'N'),
  (11, 'user11', 'user11@acu.edu', 'N'),
  (12, 'user12', 'user12@acu.edu', 'N'),
  (13, 'user13', 'user13@acu.edu', 'N'),
  (14, 'user14', 'user14@acu.edu', 'N'),
  (15, 'user15', 'user15@acu.edu', 'N'),
  (16, 'user16', 'user16@acu.edu', 'N'),
  (17, 'user17', 'user17@acu.edu', 'N'),
  (18, 'user18', 'user18@acu.edu', 'N'),
  (19, 'user19', 'user19@acu.edu', 'N'),
  (20, 'user20', 'user20@acu.edu', 'N'),
  (21, 'user21', 'user21@acu.edu', 'N'),
  (22, 'user22', 'user22@acu.edu', 'N'),
  (23, 'user23', 'user23@acu.edu', 'N'),
  (24, 'user24', 'user24@acu.edu', 'N'),
  (25, 'user25', 'user25@acu.edu', 'N'),
  (26, 'user26', 'user26@acu.edu', 'N'),
  (27, 'user27', 'user27@acu.edu', 'N'),
  (28, 'user28', 'user28@acu.edu', 'N'),
  (29, 'user29', 'user29@acu.edu', 'N'),
  (30, 'user30', 'user30@acu.edu', 'N'),
  (31, 'user31', 'user31@acu.edu', 'N'),
  (32, 'user32', 'user32@acu.edu', 'N'),
  (33, 'user33', 'user33@acu.edu', 'N'),
  (34, 'user34', 'user34@acu.edu', 'N'),
  (35, 'user35', 'user35@acu.edu', 'N'),
  (36, 'user36', 'user36@acu.edu', 'N'),
  (37, 'user37', 'user37@acu.edu', 'N'),
  (38, 'user38', 'user38@acu.edu', 'N'),
  (39, 'user39', 'user39@acu.edu', 'N'),
  (40, 'user40', 'user40@acu.edu', 'N');
-- TABLE: Show user with their email
select * from users order by user_id;

-- RESPONSE_AND_SURVEY --
drop table if exists response_surveys;
CREATE TABLE response_surveys (
  id int primary key,
  survey_id int,
  user_id int,
  survey_count int
);
insert into response_surveys (id, survey_id, user_id, survey_count) values
  (1, 1, 1, 1),
  (2, 8, 2, 1),
  (3, 15, 3, 4),
  (4, 2, 4, 1),
  (5, 11, 5, 1),
  (6, 1, 6, 1),
  (7, 12, 7, 1),
  (8, 2, 8, 1),
  (9, 1, 9, 1),
  (10, 15, 10, 1),
  (11, 1, 11, 2),
  (12, 14, 12, 1),
  (13, 6, 13, 1),
  (14, 13, 14, 1),
  (15, 8, 15, 1),
  (16, 2, 16, 2),
  (17, 12, 17, 4),
  (18, 2, 18, 2),
  (19, 2, 19, 2),
  (20, 2, 20, 2),
  (21, 6, 11, 2),
  (22, 11, 12, 2),
  (23, 1, 13, 1),
  (24, 2, 14, 2),
  (25, 1, 15, 2),
  (26, 2, 16, 2),
  (27, 2, 17, 2),
  (28, 2, 18, 2),
  (29, 8, 19, 2),
  (30, 2, 20, 2),
  (31, 11, 11, 1),
  (32, 1, 12, 3),
  (33, 1, 13, 3),
  (34, 13, 14, 3),
  (35, 6, 15, 3),
  (36, 2, 16, 4),
  (37, 14, 17, 4),
  (38, 2, 18, 2),
  (39, 2, 19, 4),
  (40, 6, 20, 3);

-- MANY TO MANY TABLES --
-- TABLES_SHOW INFO OF THE SURVEYS THAT WERE RESPONDED --
-- TABLE: User that did 1 survey (result shoudl be 15)
select rs.id, rs.survey_id, rs.user_id, u.user_name, rs.survey_count
from response_surveys rs
inner join users u on (rs.id = u.user_id) where survey_count=1
order by rs.id;
-- TABLE: User that did 2 survey (result shoudl be 15)
select rs.id, rs.survey_id, rs.user_id, u.user_name, rs.survey_count
from response_surveys rs
inner join users u on (rs.id = u.user_id) where survey_count=2
order by rs.id;
-- TABLE: User that did 3 survey (result shoudl be 5)
select rs.id, rs.survey_id, rs.user_id, u.user_name, rs.survey_count
from response_surveys rs
inner join users u on (rs.id = u.user_id) where survey_count=3
order by rs.id;
-- TABLE: User that did 4 survey (result shoudl be 5)
select rs.id, rs.survey_id, rs.user_id, u.user_name, rs.survey_count
from response_surveys rs
inner join users u on (rs.id = u.user_id) where survey_count=4
order by rs.id;

-- EOF mxr17e_dsd.sql

-- EXTRA

-- TO CREATE JSON --
-- select * from survey;
-- select row_to_json(survey) from survey;

-- FOR BOSS --
-- drop table role;
drop table if exists role;
create table role (name text, password text);
insert into role (name,password) values ('boss','blee');
select * from role;
GRANT ALL ON question TO boss;
GRANT ALL ON survey TO boss;
GRANT ALL ON author TO boss;
ALTER ROLE boss SUPERUSER;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO boss;
